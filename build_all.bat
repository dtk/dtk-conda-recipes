@echo off

cd dtk-log
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
cd ../dtk-fonts
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
cd ../dtk-widgets
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
cd ../dtk-visualization
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
cd ../dtk-core
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
cd ../dtk-logger
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
cd ../dtk
E:\conda\Scripts\conda-build.exe --no-force-upload --user dtk -c dtk .
