#!/bin/bash

if [[ -d build ]]; then
    rm -rf build
fi
mkdir build
cd build

# if [[ "$c_compiler" == "gcc" ]]; then
#   export PATH="${PATH}:${BUILD_PREFIX}/${HOST}/sysroot/usr/lib:${BUILD_PREFIX}/${HOST}/sysroot/usr/include"
# fi

cmake ../src \
  -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
  -DCMAKE_INSTALL_LIBDIR=lib \
  -DCMAKE_BUILD_TYPE=Release \
  -DMP_UNITS_API_NO_CRTP:BOOL=OFF

make -j${CPU_COUNT}
make install
