@echo off

mkdir build
cd build

set BUILD_CONFIG=Release

cmake ../src  -G "Visual Studio 17 2022" ^
    -Wno-dev ^
    -DCMAKE_INSTALL_PREFIX=%LIBRARY_PREFIX% ^
    -DCMAKE_PREFIX_PATH=%LIBRARY_PREFIX% ^
    -DCMAKE_INSTALL_RPATH:STRING=%LIBRARY_LIB% ^
    -DMP_UNITS_WARNINGS_AS_ERRORS=OFF ^
    -DMP_UNITS_BUILD_DOCS=OFF  ^
    -DMP_UNITS_BUILD_LA=OFF  ^
    -DCMAKE_CXX_STANDARD=20  ^
    -DCMAKE_CXX_STANDARD_REQUIRED=ON  ^
    -DCMAKE_INSTALL_NAME_DIR=%LIBRARY_LIB%

if errorlevel 1 exit 1

cmake --build . --verbose --config %BUILD_CONFIG% --target install
if errorlevel 1 exit 1
