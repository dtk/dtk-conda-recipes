# dtk-conda-recipes
conda recipes for building dtk, dtk layers and plugins

# how to build and upload a new package ?
build on an old distrib (to use an old glibc), currently centos 7 (available on ci.inria.fr)

## install miniconda:

```bash
wget https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh
bash ./Miniconda-latest-Linux-x86_64.sh
conda install conda-build anaconda-client conda-verify mamba boa
```

on linux, you may need to create the gcc environement if you want to use conda own gcc :
 `conda env create -f gcc.yaml`
 
on linux and macos, create the notify environment to let the jobs send notifications to discord 
 `conda env create -f notify.yaml`

## build a recipe:
```bash
conda build . -c conda-forge
# or conda mambabuild . -c conda-forge to build using mamba
```

```bash

## upload to dtk channel on anaconda:
anaconda upload -u dtk-forge /home/nniclaus/miniconda2/conda-bld/linux-64/dtk-visual-programming-1.0.1-0.tar.bz2
anaconda upload -u dtk-forge /home/nniclaus/miniconda2/conda-bld/linux-64/dtk-1.3.1-0.tar.bz2
anaconda upload -u dtk-forge /home/nniclaus/miniconda2/conda-bld/linux-64/dtk-imaging-0.1.2-0.tar.bz2
```

## upload automatique:

créer un fichier .condarc contenant :

```yaml
anaconda_upload: True
```

et ajouter l'option *--user dtk-forge* à `conda build`

## upload to private repo on mc.inria.fr

```bash
scp <*package*>.tar.bz2 mc:/var/www/conda/dream/<linux-64|osx-64|win-64>
```

sur mc en tant que user **conda**

```bash
cd /var/www/conda
for i in `ls dream`; do echo $i; conda index dream/$i ;done
```


# error Qt platform failed on windows:

```bash
set QT_QPA_PLATFORM_PLUGIN_PATH=C:/Users/<username>/Miniconda3/envs/<envname>/Library/plugins
```
