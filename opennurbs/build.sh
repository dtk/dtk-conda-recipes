#!/bin/bash

mkdir build
cd build

ARCH=`arch`
if [ $ARCH = "arm64" ] || [ $ARCH = "aarch64" ]; then
  export QT_HOST_PATH=$PREFIX
fi

    cmake .. \
          -G 'Ninja' \
          -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
          -DCMAKE_INSTALL_LIBDIR=lib \
          -DCMAKE_BUILD_TYPE=Release

cmake --build . --target install
