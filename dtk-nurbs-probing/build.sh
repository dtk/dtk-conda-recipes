#!/bin/bash

mkdir build
cd build
cmake .. \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}"  -DDTK_NURBS_PROBING_BUILD_MREP=ON

make -j${CPU_COUNT}
make install
