#!/bin/bash
rm -fr ${HOME}/custom_ssh
mkdir ${HOME}/custom_ssh
ssh-keygen -f ${HOME}/custom_ssh/ssh_host_rsa_key -N '' -t rsa
ssh-keygen -f ${HOME}/custom_ssh/ssh_host_dsa_key -N '' -t dsa
echo "Port 2223
HostKey ${HOME}/custom_ssh/ssh_host_rsa_key
HostKey ${HOME}/custom_ssh/ssh_host_dsa_key
AuthorizedKeysFile  .ssh/authorized_keys
ChallengeResponseAuthentication no
UsePAM yes
Subsystem   sftp    /usr/lib/ssh/sftp-server
PidFile ${HOME}/custom_ssh/sshd.pid" > ${HOME}/custom_ssh/sshd_config
/usr/sbin/sshd -f ${HOME}/custom_ssh/sshd_config
echo "
--------
Process ID : ${HOME}/custom_ssh/sshd.pid
-------"
sleep 3600
