
# # that is necessary, otherwise the compilation will fail, because it is hardcoded to copy the docs/html
# mkdir -p $SRC_DIR/docs/html

if [[ -d build ]]; then
    rm -rf build
fi
mkdir build
cd build

cmake .. -G 'Ninja' \
    -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DCMAKE_BUILD_TYPE=Release

cmake --build . --target install
