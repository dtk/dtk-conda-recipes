#!/bin/bash

mkdir build
cd build
if [ `uname` = "Darwin" ]; then
    cmake .. \
          -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DCMAKE_INSTALL_LIBDIR=lib -DZLIB_LIBRARY_RELEASE=$PREFIX/lib/libz.dylib
else
    cmake .. \
          -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DCMAKE_INSTALL_LIBDIR=lib -DZLIB_LIBRARY_RELEASE=$PREFIX/lib/libz.so
fi

make -j${CPU_COUNT}
make install
