#!/bin/bash

mkdir build;
meson -Dprefix="$PREFIX" build;
ninja -C build install;
