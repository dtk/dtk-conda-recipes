#!/bin/bash

mkdir build
cd build
cmake .. \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
      -DDTK_CONTINUOUS_GEOMETRY_BUILD_COMPOSER=ON

make -j${CPU_COUNT}
make install
