#!/bin/bash

# gstreamer expects libffi to be in lib64 for some reason.
#mkdir $PREFIX/lib64
#cp -r $PREFIX/lib/libffi* $PREFIX/lib64

# The datarootdir option places the docs into a temp folder that won't
# be included in the package (it is about 12MB).
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$CONDA_PREFIX/lib/pkgconfig

mkdir build;
# disabled because may be found on the system, this might not be enough
meson -Dgdk-pixbuf=disabled -Dcairo=disabled -Dtests=disabled -Dsoup=disabled -Dqt5=disabled -Dgtk3=disabled -Dprefix="$PREFIX" build;
ninja -C build install;
