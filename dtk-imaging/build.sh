#!/bin/bash

mkdir build
cd build
cmake .. \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}"  -DDTK_IMAGING_BUILD_COMPOSER=ON -DDTK_IMAGING_BUILD_WIDGETS=ON -DCMAKE_INSTALL_LIBDIR=lib

make -j${CPU_COUNT}
make install
