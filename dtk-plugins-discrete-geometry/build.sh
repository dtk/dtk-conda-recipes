#!/bin/bash

mkdir build
cd build
cmake .. \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
      -DBUILD_VTK_PLUGINS=ON \
#      -DBUILD_CGAL_PLUGINS=ON \
      -DBUILD_METIS_PLUGINS=ON
#      -DBUILD_TETGEN_PLUGINS=ON

make -j${CPU_COUNT}
make install
