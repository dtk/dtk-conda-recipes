#!/bin/bash

cd src/cmbuild
cmake .. \
      -DHYPRE_INSTALL_PREFIX="${PREFIX}" \
      -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-fPIC -O2 -g -DNDEBUG " \
      -DCMAKE_C_FLAGS_RELWITHDEBINFO="-fPIC -O2 -g -DNDEBUG " \
      -DHYPRE_BUILD_TYPE=RelWithDebInfo \
      -DCMAKE_C_COMPILER=mpicc \
      -DCMAKE_CXX_COMPILER=mpiCC

make -j${CPU_COUNT}
make install
