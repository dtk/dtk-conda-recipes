#!/bin/sh

python setup.py install --ignore-git \
                        --build-tests \
                        --verbose-build
