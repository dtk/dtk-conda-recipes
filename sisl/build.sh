#!/bin/bash

mkdir build
cd build
cmake .. \
      -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
      -DCMAKE_INSTALL_PREFIX="${PREFIX}"

make -j${CPU_COUNT}
make install
